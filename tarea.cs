using System;
public class sucursal{
	public int numeroDeSucursal;
	public int montoDeVenta;
	public int cantidadDeEmpleados;
	public int cantidadDeClientesQueCompraronEseMes;
	public string nombre;

	public void ingresarNumeroDeSucursal(int numero) {
		numeroDeSucursal = numero;
		switch (numero){
			case 1:
				nombre = "Once";
				break;
			case 2:
				nombre = "San Nicolas";
				break;
			case 3:
				nombre = "Almagro";
				break;
			case 4:
				nombre = "Microcentro";
				break;
			case 5:
				nombre = "Palermo";
				break;
			case 6:
				nombre = "San Cristobal";
				break;
			case 7:
				nombre = "Parque Patricios";
				break;
		}
	}
	
	public void ingresarMontoDeVenta(int numero) {
		montoDeVenta = numero;
	}
	public void ingresarCantidadDeEmpleados(int numero) {
		cantidadDeEmpleados = numero;
	}
	public void ingresarCantidadDeClientesQueCompraronEseMes(int numero) {
		cantidadDeClientesQueCompraronEseMes = numero;
	}
	
	
	public int obtenerNumeroDeSucursal() {
		return numeroDeSucursal;
	}
	public int obtenerMontoDeVenta() {
		return montoDeVenta;
	}
	public string obtenerNombre() {
		return nombre;
	}
	public int obtenerCantidadDeClientesQueCompraronEseMes() {
		return cantidadDeClientesQueCompraronEseMes;
	}
	public int obtenerCantidadDeEmpleados() {
		return cantidadDeEmpleados;
	}
	
	
	public void mostrar() {
		Console.WriteLine("Esta es la sucursal "+numeroDeSucursal+" ("+nombre+"), tenemos "+cantidadDeEmpleados+" empleados y vendimos"+montoDeVenta+"$ entre "+cantidadDeClientesQueCompraronEseMes+" personas");
	}
}

public class Program{
	public static void Main(){
		sucursal[] sucursales = new sucursal[7];
		for (int i=0;i<7;i++){
			sucursales[i] = new sucursal();
			Console.WriteLine("Ingrese el numero de sucursal: ");
			sucursales[i].ingresarNumeroDeSucursal(Convert.ToInt32(Console.ReadLine()));
			Console.WriteLine("Ingrese la cantidad de empleados: ");
			sucursales[i].ingresarCantidadDeEmpleados(Convert.ToInt32(Console.ReadLine()));
			Console.WriteLine("Ingrese el monto de venta: ");
			sucursales[i].ingresarMontoDeVenta(Convert.ToInt32(Console.ReadLine()));
			Console.WriteLine("Ingrese la cantidad de clientes que compraron ene se mes: ");
			sucursales[i].ingresarCantidadDeClientesQueCompraronEseMes(Convert.ToInt32(Console.ReadLine()));
		}

		for (int i=0;i<7;i++){
			sucursales[i].mostrar();
		}

		int[] aux1 = {
			sucursales[0].obtenerMontoDeVenta(),
			sucursales[1].obtenerMontoDeVenta(),
			sucursales[2].obtenerMontoDeVenta(),
			sucursales[3].obtenerMontoDeVenta(),
			sucursales[4].obtenerMontoDeVenta(),
			sucursales[5].obtenerMontoDeVenta(),
			sucursales[6].obtenerMontoDeVenta()};
		Console.WriteLine("El promedio de ventas es de: "+ejer1(aux1)+"$");

		int[] aux2 = {
			sucursales[0].obtenerCantidadDeEmpleados(),
			sucursales[1].obtenerCantidadDeEmpleados(),
			sucursales[2].obtenerCantidadDeEmpleados(),
			sucursales[3].obtenerCantidadDeEmpleados(),
			sucursales[4].obtenerCantidadDeEmpleados(),
			sucursales[5].obtenerCantidadDeEmpleados(),
			sucursales[6].obtenerCantidadDeEmpleados()};
		Console.WriteLine("El promedio de empleados es de: "+ejer2(aux2));

		int[] aux3 = {
			sucursales[0].obtenerMontoDeVenta(),
			sucursales[1].obtenerMontoDeVenta(),
			sucursales[2].obtenerMontoDeVenta(),
			sucursales[3].obtenerMontoDeVenta(),
			sucursales[4].obtenerMontoDeVenta(),
			sucursales[5].obtenerMontoDeVenta(),
			sucursales[6].obtenerMontoDeVenta()};
		Console.WriteLine("Las ventas totales son de: "+ejer3(aux1)+"$");

		Console.WriteLine("La sucursal con mas ventas es la nro"+(ejer4(aux1)+1)+"("+sucursales[ejer4(aux1)+1].obtenerNombre()+") con: "+sucursales[ejer4(aux1)+1].obtenerMontoDeVenta()+"$");

		Console.WriteLine("La sucursal con menos ventas es la nro"+(ejer5(aux1)+1)+"("+sucursales[ejer5(aux1)+1].obtenerNombre()+") con: "+sucursales[ejer5(aux1)+1].obtenerMontoDeVenta()+"$");
	}
	
	public static int ejer1(int[] entrada){ //Calculo del promedio de ventas
		int sum = 0;
		for (int i=0;i<7;i++){
			sum+=entrada[i];
		}
		sum/=7;
		return sum;
	}
	
	public static int ejer2(int[] entrada){	//Calculo del promedio de empleados
		int sum = 0;
		for (int i=0;i<7;i++){
			sum+=entrada[i];
		}
		sum/=7;
		return sum;
	}
	
	public static int ejer3(int[] entrada){	//Calculo de la venta total
		int sum = 0;
		for (int i=0;i<7;i++){
			sum+=entrada[i];
		}
		return sum;
	}
	
	public static int ejer4(int[] entrada){	//Determinar la venta máxima y que sucursal es.
		int indice = 0;
		for (int i=0;i<7;i++){
			if (entrada[indice]<entrada[i]){
				indice = i;
			}
		}
		return indice;
	}
	
	public static int ejer5(int[] entrada){	//Determinar la venta mínima y que sucursal es.
		int indice = 0;
		for (int i=0;i<7;i++){
			if (entrada[indice]>entrada[i]){
				indice = i;
			}
		}
		return indice;
	}
}